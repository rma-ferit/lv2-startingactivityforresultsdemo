package brunozoric.ferit.hr.acitivtyforresultdemo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_data_request.*

class DataRequestActivity : AppCompatActivity() {

    companion object {
        const val REQUEST_EMAIL: Int = 10
        const val KEY_EXTRA_EMAIL: String = "email"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_data_request)
        setUpUi()
    }

    private fun setUpUi() = emailAcquisitionAction.setOnClickListener{ startActivityForEmail()}

    private fun startActivityForEmail() {
        val acquireEmailIntent = Intent(this, EmailAcquisitionActivity::class.java)
        startActivityForResult(acquireEmailIntent, REQUEST_EMAIL)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when(requestCode){
            REQUEST_EMAIL->{ emailDisplay.text = data?.getStringExtra(KEY_EXTRA_EMAIL) ?: "" }
        }
    }
}
