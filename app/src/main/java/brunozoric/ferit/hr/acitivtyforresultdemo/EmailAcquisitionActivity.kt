package brunozoric.ferit.hr.acitivtyforresultdemo

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_email_acquisition.*
import android.content.Intent



class EmailAcquisitionActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_email_acquisition)
        setUpUi()
    }

    private fun setUpUi() =  emailSaveAction.setOnClickListener{ returnEmail() }

    private fun returnEmail() {
        val email = emailInput.text.toString()
        val resultIntent = Intent()
        resultIntent.putExtra(DataRequestActivity.KEY_EXTRA_EMAIL, email)
        this.setResult(Activity.RESULT_OK, resultIntent)
        this.finish()
    }
}
